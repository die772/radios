// Meteor.subscribe('radios');

Template.Radios.onCreated(function(){
	var self = this;
	self.autorun(function (){
		// Lo que hace es des-subcribirse de una
		//Radio previa, para no quedar ligado a la misma
		self.subscribe('radios');
	});
});

/*console.log(Meteor.settings.public.ga.account);*/
Template.Radios.helpers({
	Radios: ()=> {
		// Con esta consulta buscamos de en forma desc
		return Radios.find({}, { sort: { createdAt: -1 } });
		//return Radios.find({});
	}
});

Template.Radios.events({
  'click .new-radio'() {
    Session.set('newRadio', true);
  },
});
