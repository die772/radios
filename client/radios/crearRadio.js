Template.crearRadio.events({
  'click .fa-close'(){
    FlowRouter.go('lista-radios');
  },
  'submit #insertRadioForm': function (event) {
    event.preventDefault();

    var radio = {
			nom: event.target.nom.value,
			desc: event.target.desc.value,
			logo: event.target.logo.value,
      dir: event.target.dir.value,
      sitio_url: event.target.sitio_url.value,
			streaming_url: event.target.streaming_url.value,
			tipo_streaming: event.target.tipo_streaming.value,
    };

    Meteor.call("crearRadiosService", radio,function(error, result) {
      if(error) {
        $(event.target).find(".error").html(error.reason);
      } else {
        FlowRouter.go("lista-radios");
      }
    });
  }
});
