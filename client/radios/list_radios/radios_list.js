
Meteor.subscribe("radios");

var radiosList = new ReactiveVar([]);

Template.RadiosList.onCreated(function() {
	radiosList.set(Radios.find({}));
});

Template.RadiosList.helpers({
	// isAutor() {
  //   return this.autor === Meteor.userId();
  // },
	list: function() {
		return radiosList.get();
	},
	hasItem: function() {
		return radiosList.get().count();
	},

});

// Template.RadiosList.events({
// 	'click .toggle-private'() {
//     Meteor.call('Radios.setPrivado', this._id, !this.privado);
//   },
// });


Template.Search.events({
	"keyup input": _.debounce(function(e) {
		var nom = $("[name='nom']").val().trim(),
		desc = $("[name='desc']").val().trim(),
		search = {};
		if(nom)
			search.nom = {
				$regex: new RegExp(nom),
				$options: "i"
			};
			if(desc)
				search.desc = {
					$regex: new RegExp(desc),
					$options: "i"
				};

		radiosList.set(Radios.find(search, {sort: {createdAt: -1}}));
		}, 200)
});
