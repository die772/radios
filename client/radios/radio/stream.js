
Template.stream.onCreated(function(){

  this.playMode = new ReactiveVar(false);
});


Template.stream.helpers({

  playMode() {
    return Template.instance().playMode.get();
  },

});



// Esta es una mejor forma de utilizar una variable reactiva, en la funcion
// le pasamos el evento y una instancia del template
// Esto es similar a la forma que teniamos con la Session, pero la mejora
// es que tiene alcance solo en un template especifico
Template.stream.events({

  'click .radio_boton'(event, instance) {
      if (instance.playMode.get() != false) {
        $("#audio_player").trigger('pause');
        $("#audio_player").prop("currentTime", 0);
        //Just reload
        $("#audio_player").trigger('load');
        $('#estado_radio').css('background', "" );
        instance.playMode.set(!instance.playMode.get());
      } else {
        $("#audio_player").trigger('play');
        $('#estado_radio').css('background', " #04c4d9");
        instance.playMode.set(!instance.playMode.get());
      }
    }
});
