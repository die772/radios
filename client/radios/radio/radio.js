// Lo primero que queremos hacer es crear una sesion cuanto se crea el template.
Template.radio.onCreated(function(){
  this.editMode = new ReactiveVar(false);
  /*this.editMode = new ReactiveVar();
  this.editMode.set(false); Esto tambien es valido*/
  this.playMode = new ReactiveVar(false);
});


Template.radio.helpers({
	// Vamos a pasar como parametro updateradioId que es referenciado en radio.html
	// creamos una funcion que retorna el id de una radio en particular
	updateRadioId: function(){
		return this._id;
	},
  radio_id: function(){
		return this._id;
	},
  // Vamos a utilizar una variable reactiva en un instancia del template al momento
  //de la creacion y tenemos que utilzar en forma adicional un helper en el mismo template
  // que retorne el valor actual de editMode
  editMode() {
    return Template.instance().editMode.get();
  },
  playMode() {
    return Template.instance().playMode.get();
  },
  isAutor() {
    return this.autor === Meteor.userId();
  },
});


Template.radio.events({
  'click .fa-trash'(){
  	Meteor.call('Radios.deleteRadio', this._id);
  },
  // Ahora vamos a manejar la session, para que en el evento click del pencil
  // cambie el valor de editMode a lo opuesto del valor actual
  /*'click .fa-pencil'(){
  	Session.set('editMode', !Session.get('editMode'));
  }*/

});
// Esta es una mejor forma de utilizar una variable reactiva, en la funcion
// le pasamos el evento y una instancia del template
// Esto es similar a la forma que teniamos con la Session, pero la mejora
// es que tiene alcance solo en un template especifico
Template.radio.events({
  'click .fa-pencil'(event, instance) {

    instance.editMode.set(!instance.editMode.get());
  },
  // 'submit #updateRadioId': function (event) {
  //   event.preventDefault();
  //   var radio = {
	// 		nom: event.target.nom.value,
	// 		desc: event.target.desc.value,
	// 		logo: event.target.logo.value,
  //     dir: event.target.dir.value,
  //     sitio_url: event.target.sitio_url.value,
	// 		streaming_url: event.target.streaming_url.value,
	// 		tipo_streaming: event.target.tipo_streaming.value,
  //   };
  //
  //   Meteor.call("updateRadiosService", radio,function(error, result) {
  //     if(error) {
  //       $(event.target).find(".error").html(error.reason);
  //     } else {
  //       FlowRouter.go("lista-radios");
  //     }
  //   });
  // },
  'click .toggle-private'() {
      Meteor.call('Radios.setPrivado', this._id, !this.privado);
  },
  'click .fa-play'(event, instance) {
      if (instance.playMode.get() != false) {
        $('#' + this._id).trigger('pause');
        $('#' + this._id).prop("currentTime", 0);
        //Just reload
        $('#' + this._id).trigger('load');
        $('#fa-' + this._id).css('background', "" );
        instance.playMode.set(!instance.playMode.get());
      } else {
        $('#' + this._id).trigger('play');
        $('#fa-' + this._id).css('background', " rgb(21, 18, 3)");
        instance.playMode.set(!instance.playMode.get());
      }
    }
});
