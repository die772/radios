
Meteor.subscribe("comentario");

var comentariosList = new ReactiveVar([]);

Template.ComentariosList.onCreated(function() {
  var id = FlowRouter.getParam('id');
	comentariosList.set(Comentarios.find({radioId: id}));
});

Template.ComentariosList.helpers({
	list: function() {
		return comentariosList.get();
	},
	hasItem: function() {
		return comentariosList.get().count();
	},

});
