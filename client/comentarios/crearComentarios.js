Template.crearComentarios.onCreated(function(){
	var self = this;
	self.autorun(function (){
		// El parametro que obtuvimos es el id de una radio
		var id_radio = FlowRouter.getParam('id');
		// Ahora nos vamos a subscribir a las Comentarios de una radio en particular
		self.subscribe('comentarios', id_radio);
	});
});

Template.crearComentarios.events({
	"submit #insertComentariosForm": function (event) {
		event.preventDefault();
		var id_radio = FlowRouter.getParam('id');

		var comentario = {
			titulo: event.target.titulo.value,
			desc: event.target.desc.value,
			imagen: event.target.imagen.value,
		};

		Meteor.call("crearComentariosService", comentario, id_radio,function(error, result) {
			if(error) {
				$(event.target).find(".error").html(error.reason);
				alert("Usuario no Autorizado");
			}
		});

		// Tengo que realizar un clear del form
		event.target.titulo.value="";
		event.target.desc.value="";
		event.target.imagen.value="";

		return false;
	}
});
