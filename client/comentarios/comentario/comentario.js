// Lo primero que queremos hacer es crear una sesion cuanto se crea el template.
Template.comentario.onCreated(function(){
  this.editMode = new ReactiveVar(false);
  /*this.editMode = new ReactiveVar();
  this.editMode.set(false); Esto tambien es valido*/
});


Template.comentario.helpers({
	// Vamos a pasar como parametro updateradioId que es referenciado en radio.html
	// creamos una funcion que retorna el id de una radio en particular
	updateComentarioId: function(){
    return this._id
	},
  // Vamos a utilizar una variable reactiva en un instancia del template al momento
  //de la creacion y tenemos que utilzar en forma adicional un helper en el mismo template
  // que retorne el valor actual de editMode
  editMode() {
    return Template.instance().editMode.get();
  },
});


Template.comentario.events({

  'click .fa-trash'(){
  	Meteor.call('Comentarios.deleteComentario', this._id);
  },
  // Ahora vamos a manejar la session, para que en el evento click del pencil
  // cambie el valor de editMode a lo opuesto del valor actual
  /*'click .fa-pencil'(){
  	Session.set('editMode', !Session.get('editMode'));
  }*/

});
// Esta es una mejor forma de utilizar una variable reactiva, en la funcion
// le pasamos el evento y una instancia del template
// Esto es similar a la forma que teniamos con la Session, pero la mejora
// es que tiene alcance solo en un template especifico
Template.comentario.events({
  'click .fa-pencil'(event, instance) {
    // increment the counter when button is clicked
    instance.editMode.set(!instance.editMode.get());
  },
});
