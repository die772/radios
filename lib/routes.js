/******************************************************************/
// Actualmente tenemos un problema, al logearnos no
// nos redirecciona en forma automatica al libro de recetas,
// para solucionarlo usamos el paquete gwendall:auth-client-callbacks
Accounts.onLogin(function(){
	FlowRouter.go('lista-radios');
});

// Al deslogearse sucede lo mismo, nos tiene que redirigir en forma automatica
// a la home page (paquete gwendall:auth-client-callbacks)
Accounts.onLogout(function(){
	FlowRouter.go('home');
});


FlowRouter.route('/', {
	name: "home",
	action(){
		// Si el usuario esta logeado, entonces lo
		// redireccionamos a la radio
		if (Meteor.userId()){
			FlowRouter.go('/lista-radios');
		}
		BlazeLayout.render('HomeLayout');
	}
});

FlowRouter.route('/lista-radios', {
	name: "lista-radios",
	action(){
		BlazeLayout.render('MainLayout', {main:'RadiosList'});
	}
});

FlowRouter.route('/crearRadio', {
	name: "radio",
	action(){
		BlazeLayout.render('MainLayout', {main:'crearRadio'});
	}
});
/* Cambios para una noticia */
FlowRouter.route('/crearComentarios/:id', {
	name: "comentario",
	action(){
		BlazeLayout.render('MainLayout', {main:'crearComentarios'});
	}
});

// Si el usuario no esta logeado no queremos
// que pueda acceder a las radios favoritas,
// queremos que se quede en home
FlowRouter.triggers.enter([function(context, redirect){
	if (!Meteor.userId()){
			FlowRouter.go('home');
	}
}]);
