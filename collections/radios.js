

Radios = new Mongo.Collection('radios');

/* Habilita la insercion si y solo si existe el usuario */
Radios.allow({
  insert: function(userId, doc){
    return !!userId;
  },
  //Habilitamos el update a un usuario
	update: function(userId, doc){
		return !!userId;
	},

});

/*Primer Esquema creado RadiosSchema */
RadiosSchema = new SimpleSchema({
  nom:{
    type: String,
    label: "Nombre de la Radio"
  },
  desc: {
    type: String,
    label:"Descripción"
  },
  logo:{
    type: String,
    label:"Logo",
    optional: true
  },
  dir:{
    type: String,
    label:"Dirección",
    optional: true,
  },
  sitio_url:{
    type: String,
    label:"Sitio Web",
  },
  streaming_url:{
    type: String,
    label: "Streaming",
  },
  tipo_streaming: {
        type: String,
        label:"Tipo de Streaming",
        allowedValues: ['audio/x-mpegurl','audio/mpeg','audio/mp3','audio/ogg','audio/wave','audio/wav','audio/x-wav','audio/flac','audio/x-flac'],
        optional: true,
        autoform:{
          type: "select-radio-inline"
        }
  },
  privado:{
    type: Boolean,
    defaultValue: false,
    optional: true,
    autoform: {
      type: "hidden",
    }
  },
  autor: {
    type: String,
    label:"Autor",
    // autoValue: function (){
    //   return this.userId
    // },
    autoform:{
      type:"hidden"
    }
  },
  createAt:{
    type: Date,
    label: "Creado en",
    // autoValue: function(){
    //   return new Date()
    // },
    autoform:{
      type:"hidden"
    }
  }
});

Radios.attachSchema( RadiosSchema );
