import { Meteor } from 'meteor/meteor';

Comentarios = new Mongo.Collection('comentarios');

/* Habilita la insercion si y solo si existe el usuario */
Comentarios.allow({
  insert: function (userId, doc) {
      return !!userId;
    },
    update: function (userId, doc) {
      return !!userId;
    },
    // update: function (userId, doc, fieldNames, modifier) {
    //   return !!userId;
    // },

    remove: function (userId, doc) {
      return false;
    }
});

ComentariosSchema= new SimpleSchema({
  titulo:{
    type: String,
    label: "Titulo del Comentario"
  },
  desc: {
    type: String,
    label:"Descripción"
  },
  imagen:{
    type: String,
    label:"Imagen",
    optional: true,
  },
  autor: {
    type: String,
    label:"Autor",
    autoValue: function (){
      return this.userId
    },
    autoform:{
      type:"hidden"
    }
  },
  email: {
    type: String,
    label:"Email",
    autoform:{
      type:"hidden"
    }
  },
  createAt:{
    type: Date,
    label: "Creado en",
    autoValue: function(){
      return new Date()
    },
    autoform:{
      type:"hidden"
    }
  },
  radioId:{
      type: String,
      label: "radio",
      max: 200,
      autoform:{
        type:"hidden"
      }
  }
});

Comentarios.attachSchema( ComentariosSchema );
