
Meteor.publish('radios', function() {
    return Radios.find({
      $or: [
        { privado: { $ne: true } },
        { autor: this.userId },
      ],
    });
  });

Meteor.publish('comentarios', function (id_radio) {
	check(id_radio, String);
	// return Comentarios.find({radioId: id_radio},{autor: this.userId},{sort: {createdAt: -1}});
  // retorno todos los comentarios sin discriminar por usuario
  return Comentarios.find({radioId: id_radio},{sort: {createdAt: -1}});

});
