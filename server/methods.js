// Metodos
Meteor.methods({
	crearRadiosService: function(radio) {
		radio.createAt = new Date();
		radio.autor = this.userId;
		radio.privado = false;
		check(radio, RadiosSchema);

		if (!this.userId) {
      throw new Meteor.Error('Usuario no logeado');
    }

		Radios.insert(radio);
	},
	updateRadiosService: function(radio, docId) {
		check(radio, RadiosSchema);
		const radio_aux = Radios.findOne(docId);
		if (radio_aux.autor !== this.userId) {
			throw new Meteor.Error('Usuario no autorizado');
		}
		Radios.update(docId,radio);
	},
	'Radios.setPrivado'(radioId, setToPrivado) {
		check(radioId, String);
		check(setToPrivado, Boolean);
		const radio = Radios.findOne(radioId);
		if (radio.autor !== this.userId) {
			throw new Meteor.Error('No Autorizado');
		}
		Radios.update(radioId, { $set: { privado: setToPrivado } });
	},
	'Radios.deleteRadio'(id) {
		check(id, String);
		const radio = Radios.findOne(id);
		if (radio.autor !== this.userId) {
			throw new Meteor.Error('Usuario no autorizado');
		}
		Radios.remove(id);
	}
});

Meteor.methods({
	// Voy a permitir que la creacion de los Comentarios las realize cualquier Usuario
	// pero la modificacion debe realizarse por el mismo usuario que la creo en primer lugar
	// lo mismo para eliminar
	crearComentariosService: function(comentario, id_radio) {
		check(id_radio, String);
		comentario.createAt = new Date();
		comentario.autor = this.userId;
		comentario.email = Meteor.users.findOne(this.userId).emails[0].address;
		comentario.radioId = id_radio;
		check(comentario, ComentariosSchema);

		Comentarios.insert(comentario);
	},
	updateComentariosService: function(comentario, docId) {
		check(comentario, ComentariosSchema);
		const comen_aux = Comentarios.findOne(docId);
		if (comen_aux.autor !== this.userId) {
			throw new Meteor.Error('Usuario no autorizado');
		}

		Comentarios.update(docId, comentario);
	},
	'Comentarios.deleteComentario'(id) {
		check(id, String);
		const comentario = Comentarios.findOne(id);

		if (comentario.autor !== this.userId) {
			throw new Meteor.Error('Usuario no autorizado');
		}
		Comentarios.remove(id);
	},
});
