import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { assert } from 'meteor/practicalmeteor:chai';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

import '../server/publish.js';
import '../server/methods.js';
import '../collections/radios.js';


if (Meteor.isServer) {
  describe('Radios', () => {
    describe('methods Radios.deleteRadio', () => {
      const userId = Random.id();
      var  radioId;

      beforeEach(() => {
        Radios.remove({});
        console.log("Muestro el usuario "+ userId);
        radioId = Radios.insert({
          nom:'radio de prueba',
          desc:'descripcion de prueba',
          logo:'logo prueba',
          dir:'direccion',
          sitio_url:'sitio web',
          streaming_url:'streaming prueba',
          tipo_streaming:'audio/mp3',
          privado: false,
          autor: userId,
          createAt:new Date(),
        });
      });

      it('Puede eliminar una radio de la que se es propietario', () => {
        // Find the internal implementation of the task method so we can
        // test it in isolation
        const eliminarRadio = Meteor.server.method_handlers['Radios.deleteRadio'];

        // Set up a fake method invocation that looks like what the method expects
        const invocation = { userId };

        // Run the method with `this` set to the fake invocation
        eliminarRadio.apply(invocation, [radioId]);

        // Verify that the method does what we expected
        assert.equal(Radios.find().count(), 0);
      });
    });
  });
/************************************************************************/
  describe('Radios2', () => {
    describe('methods 2 Radios.deleteRadio', () => {
      const userId = Random.id();
      const userId_aux = Random.id();
      var  radioId;

      beforeEach(() => {
        Radios.remove({});
        radioId = Radios.insert({
          nom:'radio de prueba',
          desc:'descripcion de prueba',
          logo:'logo prueba',
          dir:'direccion',
          sitio_url:'sitio web',
          streaming_url:'streaming prueba',
          tipo_streaming:'audio/mp3',
          privado: false,
          autor: userId,
          createAt:new Date(),
        });
      });

      it('No puede eliminar una radio de la que no se es propietario', () => {
        // Find the internal implementation of the task method so we can
        // test it in isolation
        const eliminarRadio = Meteor.server.method_handlers['Radios.deleteRadio'];

        // Set up a fake method invocation that looks like what the method expects
        const invocation = { userId_aux };

        // Run the method with `this` set to the fake invocation
        eliminarRadio.apply(invocation, [radioId]);

        // Verify that the method does what we expected
        assert.equal(Radios.find().count(), 0);
      });
    });
  });
  /***********************************************************************

  describe('Radios3', () => {
    describe('methods  updateRadiosService', () => {
      const userId = Random.id();
      const userId_aux = Random.id();
      var  radioId;

      beforeEach(() => {
        Radios.remove({});
        radioId = Radios.insert({
          nom:'radio de prueba',
          desc:'descripcion de prueba',
          logo:'logo prueba',
          dir:'direccion',
          sitio_url:'sitio web',
          streaming_url:'streaming prueba',
          tipo_streaming:'audio/mp3',
          privado: false,
          autor: userId,
          createAt:new Date(),
        });
      });

      it('Permito la modificacion una radio de la que se es propietario', () => {
        // Find the internal implementation of the task method so we can
        // test it in isolation
        const updateRadio = Meteor.server.method_handlers['updateRadiosService'];

        // Set up a fake method invocation that looks like what the method expects
        const invocation = { userId };

        radio = Radios.findOne(radioId);

        radio = Radios.update({
          nom:'Modifico el nombre de la radio',
        });

        // Run the method with `this` set to the fake invocation
        updateRadio.apply(invocation, [radioId], radio);

        // Verify that the method does what we expected
        assert.equal(Radios.find().count(), 0);
      });
    });
  });
/************************************************************************/


}
